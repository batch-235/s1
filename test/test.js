const { assert } = require("chai");
const { newUser, user } = require("../index.js");


describe("Test newUser object", () => {
    it("Assert newUser type is an object", () => {
        assert.equal(typeof newUser, "object");
    })

    it("Assert newUser email type is a string", () => {
        assert.equal(typeof newUser.email, "string");
    })

    it("Assert newUser email is not undefined", () => {
        assert.notEqual(typeof newUser.email, "undefined");
    })

    it("Assert newUser password is a string", () => {
        assert.equal(typeof newUser.password, "string");
    })

    it("Assert newUser password is at least 16 characters long", () => {
        assert.isAtLeast(newUser.password.length, 16);
    })
});

describe("[Activity] Test user object", () => {
    it("[1] Assert user firstName type is a string", () => {
        assert.equal(typeof user.firstName, "string");
    })

    it("[2] Assert user lastName type is a string", () => {
        assert.equal(typeof user.lastName, "string");
    })

    it("[3] Assert user firstName is not undefined", () => {
        assert.notEqual(typeof user.firstName, "undefined");
    })

    it("[4] Assert user lastName is not undefined", () => {
        assert.notEqual(typeof user.lastName, "undefined");
    })

    it("[5] Assert user age is at least 18", () => {
        assert.isAtLeast(user.age, 18);
    })

    it("[6] Assert user age type is a number", () => {
        assert.equal(typeof user.age, "number");
    })

    it("[7] Assert user contactNumber type is a string", () => {
        assert.equal(typeof user.contactNumber, "string");
    })

    it("[8] Assert user batchNumber type is a number", () => {
        assert.equal(typeof user.batchNumber, "number");
    })

    it("[9] Assert user batchNumber is not undefined", () => {
        assert.notEqual(typeof user.batchNumber, "undefined");
    })

    it("[10] Assert user password is at least 16 characters", () => {
        assert.isAtLeast(user.password.length, 16)
    })
})

// npm test -- -f user